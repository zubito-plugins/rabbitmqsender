<?php
/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Zubischuhe\RabbitMQSender;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Google\Cloud\PubSub\PubSubClient;

/**
 * A streamWrapper implementation for handling `gs://bucket/path/to/file.jpg`.
 * Note that you can only open a file with mode 'r', 'rb', 'rb', 'w', 'wb', or 'wt'.
 *
 * See: http://php.net/manual/en/class.streamwrapper.php
 */
class RabbitMQSender
{
    /**
     * An indentifier
     * @var string
     */
    public $server;
    
        /**
     * An indentifier
     * @var int
     */
    public $port;
    
    
        /**
     * An indentifier
     * @var string
     */
    public $username;
    
        /**
     * An indentifier
     * @var string
     */
    public $password;


    /**
     * The service account -> json_encode
     * @var array
     */
    public $keyFileJson;
    
    
    public function __construct($keyFileJson = null, $topic = null){
        $this->keyFileJson = $keyFileJson;
        $this->topic = $topic;
    }

    public function sendToPubSub($msg, $tag) {
        $pubSub = new PubSubClient([
            'projectId' => 'zubito-262612',
            'keyFile' => (array) $this->keyFileJson
        ]);

        //Write Tags to Array 
        $tags = explode(',', $tag);

        //Create json to deliver message to Server
        $deliveryMessage = json_encode(array("body" => $msg, "tags" => $tags));

        // Get an instance of a previously created topic.
        $topic = $pubSub->topic($this->topic);

        // Publish a message to the topic.
        $response = $topic->publish([
            'data' => $deliveryMessage
        ]);

        return true;
    }

    public function sendToPubSubRaw($msg) {
         $pubSub = new PubSubClient([
            'projectId' => 'zubito-262612',
            'keyFile' => (array) $this->keyFileJson
        ]);

        // Get an instance of a previously created topic.
        $topic = $pubSub->topic($this->topic);

        // Publish a message to the topic.
        $response = $topic->publish([
            'data' => $msg
        ]);

        return true;
    }

    public function sendToPubSubMail($template, $email, $array, $portal) {
        $pubSub = new PubSubClient([
            'projectId' => 'zubito-262612',
            'keyFile' => (array) $this->keyFileJson
        ]);

        //Create json to deliver message to Server
        $deliveryMessage = json_encode(array("template" => $template, "email" => $email, "data" => $array, "portal" => $portal));

        // Get an instance of a previously created topic.
        $topic = $pubSub->topic($this->topic);

        // Publish a message to the topic.
        $response = $topic->publish([
            'data' => $deliveryMessage
        ]);

        return $response;
    }

}
